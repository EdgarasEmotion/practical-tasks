package com.helloworld;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.PollableChannel;
import org.springframework.messaging.support.GenericMessage;

@SpringBootConfiguration
public class SpringIntegrationHelloWorld {
	

	public static void main(String[] args) {

       	AbstractApplicationContext context = new ClassPathXmlApplicationContext("helloWorld.xml", SpringIntegrationHelloWorld.class);
		MessageChannel inputChannel = context.getBean("inputChannel", MessageChannel.class);
		PollableChannel outputChannel = context.getBean("outputChannel", PollableChannel.class);
		inputChannel.send(new GenericMessage<String>("World"));
		System.out.println(outputChannel.receive(0).getPayload());
		((AbstractApplicationContext) context).close();

	}

}
